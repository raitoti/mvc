package sample.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MariaDB {
    private Connection con;
    private Statement st;
    private ResultSet rs;
    static boolean result;

    public boolean connect(String database, String username, String password, boolean back) {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/"+database,username,password);
            st = con.createStatement();
            rs = st.executeQuery("select * from data");
            System.out.println("Connected\n");
            back = true;
            result = back;
            return result;
        } catch(Exception e) {
            System.out.println("Error : "+e+"\n");
            back = true;
            result = back;
            return result;
        }
    }
}
