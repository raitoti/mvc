package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class OpenScene {
    public void start(Stage window) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("view/realLayout.fxml"));
        Scene scene = new Scene(root);
        window.setScene(scene);
        window.show();
    }
}
