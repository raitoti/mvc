package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class Controller {

    private sample.model.Mysql Mysql = new sample.model.Mysql();
    private sample.model.Postgresql Post = new sample.model.Postgresql();
    private sample.model.MariaDB Maria = new sample.model.MariaDB();
    private boolean con = false;

    @FXML
    private Button connect;

    @FXML
    private TextField databaseName;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    public void connectButton(ActionEvent actionEvent) {
        System.out.println("Database    : "+comboBox.getValue());
        System.out.println("Data Name   : "+databaseName.getText());
        System.out.println("Username    : "+username.getText());
        System.out.println("Password    : "+password.getText());
        if(!"".equals(databaseName.getText()) && comboBox.getValue() != null && !"".equals(username.getText()) && !"".equals(password.getText())) {
            if(comboBox.getValue().equals("MySql")) {
                try {
                    Mysql.connect(databaseName.getText(),username.getText(),password.getText(), con);
                    if(con) {
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else if(comboBox.getValue().equals("MariaDB")) {
                try {
                    Maria.connect(databaseName.getText(),username.getText(),password.getText(), con);
                    con = true;
                    if(con) {
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    Post.connect(databaseName.getText(),username.getText(),password.getText(), con);
                    if(con) {
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @FXML
    public void comboBoxes(ActionEvent actionEvent) {
    }

    @FXML
    public void databaseName(ActionEvent actionEvent) {
    }

    @FXML
    public void username(ActionEvent actionEvent) {
    }

    @FXML
    public void password(ActionEvent actionEvent) {
    }

    @FXML
    private ComboBox<String> comboBox;

    @FXML
    void initialize() {
        assert comboBox != null : "fx:id=\"combo\" was not injected: check your FXML file 'ComboboxExample.fxml'.";
        comboBox.getItems().clear();
        comboBox.getItems().addAll("MySql","MariaDB","PostgresSQL");
    }

}